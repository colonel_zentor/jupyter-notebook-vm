from setuptools import find_packages
from distutils.core import setup

# This is about as simple of a setup.py as you can have...

setup(
    name='name-for-your-package',
    version='0.0.1',
    description='A short description for your package...something succinct.',
    long_description="""
    A much longer description...could be the README.md for your project if you
    want. It's totally up to you what you put here.
    """,
    author='John Q. Public',
    author_email='john.public@example.com',
    url='https://name-for-your-package.example.com',
    license='MIT License',
    packages=find_packages(exclude=('docs', 'setup', 'tests')),
)