### Overview

This repo provides an automated means to provision a fully working [Jupyter Notebook](http://jupyter.org/index.html) server with an accompanying [IPython MPI Cluster](http://ipyparallel.readthedocs.org/en/latest/mpi.html) both running as [supervisord](http://supervisord.org/) services.  The provisioning uses [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/) to create a [Centos 7](https://www.centos.org/) based virtual machine that exposes ports `9001`, `8888` and `22`, four processor cores and 2048 MB of ram. If you need more or less cores or ram (assuming the host machine has the desired resources), modify the `Vagrantfile` to suite your needs.

The provisioning script, `provision/provision_script`, uses [conda](http://conda.pydata.org/docs/) to create a self-contained python environment (Python 3 unless otherwise specified) that has the necessary Jupyter Notebook and IPython MPI parallel packages installed.  If you have a conda [`environment.yml`](http://conda.pydata.org/docs/using/envs.html#share-an-environment) file defined at the root of your project, the provisioning script will install the packages specified in `environment.yml` into the default vagrant conda environment. If you have not created an `environment.yml` file before, see the `example_environment.yml` for more information. 

If you have a `setup.py` file defined at the root of your project, the provisioning script will install your python package into the default conda environment in development mode.  This makes it simple to import and interact with your code via an Jupyter Notebook. If you have not created an `setup.py` file before, see the `example_setup.py` for more information. 

Also, if you do not have a `notebooks` directory at the root of your project, the provisioning script will create it for you. By default the notebook server will serve the `notebooks` directory to your browser so any notebooks you create will automatically exist on your host machine in that directory.

The provisioning scripts can be easily extend by modying the `provision/custom/custom_script` file with your own bash commands.  The `custom_script` file will automatically be run by Vagrant after the `provision/provision_script` has completed.


### Usage
 
To use, clone this repo and copy the `Vagrantfile` file and `provision` directory to the root of your project.  Assuming you have Vagrant and VirtualBox installed, simply type `vagrant up --provider virtualbox` to provision the virtual machine.  Once the provisioning is done, point your browser to [http://localhost:8888](http://localhost:8888) to access to your project via an Jupyter Notebook interface. 

Note, the notebook server is configured to use no access security. If access security is something you need, update the `create_notebook_config` function in the `provision_components` script with an access token or password. See [Security in the Jupyter notebook server](http://jupyter-notebook.readthedocs.io/en/latest/security.html) for more information.

In addition, you can control and introspect the Jupyter Notebook and IPython Cluster services via the supervisord webinterface by pointing your browser to [http://localhost:9001](http://localhost:9001).  From there you can start, stop and restart either service and view the tail of their respective output logs by clicking on the `jupyter_notebook` or `ipython_cluster` links. Note, at this time the `Tail -f` link for each service is not working.  


### Known Issues

In general this script should work without problems even if you just clone the repo and run `vagrant up`.  However in some circumstances, git on Windows changes the line endings from UNIX to DOS endings.  In those situations the Jupyter Notebook server and IPython MPI cluster services will fail to start and other random errors might occur.  Your simplest approach is to change the way git handles line endings via `git config core.autocrlf --global false`, delete the repo and reclone it.  Hopefully that will fix your problems.  If you continue to have provisioning issues, ssh into the VM via `vagrant ssh` and run `sudo journalctl -f` or seach `/var/log/message` for more details.