#!/bin/bash

#-----------------------------------------------------------------------------
#  Copyright (c) 2015, Jack Zentner (jack.zentner@gmail.com)
#
#  Distributed under the terms of the MIT License.
#-----------------------------------------------------------------------------

function bashrc_config () {
tee --append /home/${USER_NAME}/.bashrc <<EOF 
PS1='\[\033[1;32m\]\u@\h: \[\033[1;36m\]\w
:>\[\033[0m\] '
    
alias ll='ls -lF --color=auto --group-directories-first'
alias la='ls -laF --color=auto --group-directories-first'

export PATH="/opt/miniconda/bin:$PATH"
source activate ${ENV_NAME}
EOF
}


function conda_install () {
    if [ ! -d "/opt/miniconda" ]; then
        wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
        chmod +x /tmp/miniconda.sh
        /tmp/miniconda.sh -b -p /opt/miniconda
    fi

    /opt/miniconda/bin/conda config --add channels conda-forge

    if [ ! -d "/opt/miniconda/envs/${ENV_NAME}" ]; then
        if [ -f "${BASE_DIR}/environment.yml" ]; then
            /opt/miniconda/bin/conda env create -n ${ENV_NAME} --file=${BASE_DIR}/environment.yml
        else
            /opt/miniconda/bin/conda create -n ${ENV_NAME} python=3
        fi  
    fi
}


function conda_ownership(){
    chown -R ${USER_NAME}:${USER_NAME} /opt/miniconda
    chmod -R 775 /opt/miniconda
}


function environment_config () {
    source /vagrant/provision/environment_variables
    cp ${BASE_DIR}/provision/environment_variables /etc/profile.d/environment_variables.sh
}


function external_conda_evn_access () {
    # If you want IDEs like PyCharm to have access to the vag-env conda  
    # enviroment add this function to your provision script.
    sed -i 's/Subsystem/#Subsystem"/g' /etc/ssh/sshd_config
    tee --append  /etc/ssh/sshd_config <<EOF 
Subsystem sftp internal-sftp
EOF
    systemctl restart sshd
}


function firewall_config () {
    systemctl enable firewalld
    systemctl start firewalld

    # Jupyter notebook port
    firewall-cmd --zone=public --add-port=8888/tcp --permanent

    # Supervisor port
    firewall-cmd --zone=public --add-port=9001/tcp --permanent

    # Django server port
    firewall-cmd --zone=public --add-port=8000/tcp --permanent
    firewall-cmd --reload
}


function ipython_cluster_install () {
    source /opt/miniconda/bin/activate ${ENV_NAME}

    conda install ipython ipyparallel mpi4py mpich2 dill

    if [ ! -d "${IPTYHON_PROFILE_DIR}/profile_mpi" ]; then
        ipython profile create --parallel --profile=mpi --ipython-dir=${IPTYHON_PROFILE_DIR}
        echo "c.IPClusterEngines.engine_launcher_class = 'MPIEngineSetLauncher'" >> ${IPTYHON_PROFILE_DIR}/profile_mpi/ipcluster_config.py
        echo "c.IPClusterStart.controller_launcher_class = 'MPIControllerLauncher'" >> ${IPTYHON_PROFILE_DIR}/profile_mpi/ipcluster_config.py
    fi

    chown -R ${USER_NAME}:${USER_NAME} ${IPTYHON_PROFILE_DIR}
    chmod -R 775 ${IPTYHON_PROFILE_DIR}
}


function ipython_supervisord () {
    if [ -d /etc/supervisor/conf.d ]; then
        cp ${BASE_DIR}/provision/ipython/ipython_supervisord.conf  /etc/supervisor/conf.d/ipython_supervisord.conf
    fi
}


function ipython_systemd () {
    cp ${BASE_DIR}/provision/ipython/ipython-mpi-cluster.service  /etc/systemd/system

    systemctl daemon-reload
    systemctl enable ipython-mpi-cluster.service
    systemctl start ipython-mpi-cluster.service
}


function node_install () {
    # Node, npm and bower are needed for some IPython Widget related packages 
    yum install -y node npm
    npm install -g bower
}


function notebook_install () {
    source /opt/miniconda/bin/activate ${ENV_NAME}
    conda install notebook  
    conda install -c conda-forge ipywidgets

    if [ ! -f "${JUPYTER_PROFILE_DIR}/jupyter_notebook_config.py" ]; then
        create_notebook_config
    fi

    chown -R ${USER_NAME}:${USER_NAME} ${JUPYTER_PROFILE_DIR}
    chmod -R 775 ${JUPYTER_PROFILE_DIR}
}


function create_notebook_config () {
    mkdir ${JUPYTER_PROFILE_DIR}
    touch ${JUPYTER_PROFILE_DIR}/jupyter_notebook_config.py

    cat << EOF >> ${JUPYTER_PROFILE_DIR}/jupyter_notebook_config.py
c.NotebookApp.token = ''
c.NotebookApp.password = ''
c.NotebookApp.notebook_dir = '${NOTEBOOK_DIR}'
c.NotebookApp.open_browser = False
c.NotebookApp.port = 8888
c.NotebookApp.ip = '0.0.0.0'
EOF
}


function notebook_directory () {
    if [ ! -d "${BASE_DIR}/notebooks" ]; then
        mkdir ${BASE_DIR}/notebooks
        cp ${BASE_DIR}/provision/ipython/MPI_cluster_test.ipynb ${BASE_DIR}/notebooks
    fi
}


function notebook_supervisord () {
    if [ -d /etc/supervisor/conf.d ]; then
        cp ${BASE_DIR}/provision/jupyter/jupyter_supervisord.conf  /etc/supervisor/conf.d/jupyter_supervisord.conf
    fi
}


function notebook_systemd () {
    cp ${BASE_DIR}/provision/jupyter/jupyter-notebook-server.service  /etc/systemd/system

    systemctl daemon-reload
    systemctl enable jupyter-notebook-server.service
    systemctl start jupyter-notebook-server.service
}


function python_project_install () {
    cd ${BASE_DIR}
    if [ -f "${BASE_DIR}/setup.py" ]; then
        python setup.py develop
    fi
}


function supervisor_install () {
    pip install supervisor
    
    mkdir -p /var/log/supervisor
    chown -R ${USER_NAME}:${USER_NAME} /var/log/supervisor

    mkdir -p /etc/supervisor/conf.d
    cp ${BASE_DIR}/provision/supervisor/supervisord.conf /etc/supervisor

    cp ${BASE_DIR}/provision/supervisor/supervisord.service /etc/systemd/system
    systemctl enable supervisord
}


function yum_packages () {
    yum install -y epel-release deltarpm
    yum install -y git htop curl vim wget tar python-setuptools
    easy_install pip
    yum groupinstall -y 'Development Tools'
}

function yum_update () {
    yum -y update
}
